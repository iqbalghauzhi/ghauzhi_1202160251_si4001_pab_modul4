package com.example.ghauzhi_1202160251_si4001_pab_modul4;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {

    private EditText editEmail;
    private EditText editPass;
    private Button btnMasuk;
    private TextView txtDaftar;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editEmail=(EditText) findViewById(R.id.editEmail);
        editPass=(EditText) findViewById(R.id.editPass);
        btnMasuk=(Button) findViewById(R.id.btnMasuk);
        txtDaftar=(TextView) findViewById(R.id.txtDaftar);
        firebaseAuth = firebaseAuth.getInstance();

        final TextView txtDaftar = (TextView) findViewById(R.id.txtDaftar);
        txtDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Register.class);
                startActivity(intent);
            }
        });
    }


    public void Onclick(View v) {
        final ProgressDialog progressDialog = ProgressDialog.show(Login.this,
                "Harap Tunggu...", "Sedang Memproses ...",  true);
        (firebaseAuth.signInWithEmailAndPassword(editEmail.getText().toString(), editPass.getText().toString()))
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if (task.isSuccessful()) {
                            Toast.makeText(Login.this, "Login Berhasil", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(Login.this, MainActivity.class);
                            intent.putExtra("Email", firebaseAuth.getCurrentUser().getEmail());
                            startActivity(intent);
                        }

                        else {
                            Log.e("Error", task.getException().toString());
                            Toast.makeText(Login.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();

                        }
                    }
                });
    }
}
