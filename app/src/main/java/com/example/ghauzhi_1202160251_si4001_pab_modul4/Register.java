package com.example.ghauzhi_1202160251_si4001_pab_modul4;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Register extends AppCompatActivity {

    private EditText editNama;
    private EditText editEmail;
    private EditText editPass;
    private Button btnDaftar;
    private TextView txtLogin;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editNama = (EditText) findViewById(R.id.editNama);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editPass = (EditText) findViewById(R.id.editPass);
        btnDaftar = (Button) findViewById(R.id.btnDaftar);
        txtLogin = (TextView) findViewById(R.id.txtLogin);
        firebaseAuth = FirebaseAuth.getInstance();

        final TextView txtLogin = (TextView) findViewById(R.id.txtLogin);
        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Register.this, Login.class);
                startActivity(intent);
            }
        });
    }

    public void btnDaftar_Click(View v) {
        if (TextUtils.isEmpty(editEmail.getText()) || TextUtils.isEmpty(editPass.getText()) || TextUtils.isEmpty(editNama.getText())) {
            Toast.makeText(Register.this, "Data yang diminta tidak boleh kosong", Toast.LENGTH_SHORT).show();
        } else {
            //instansiasi database firebase
            FirebaseDatabase database = FirebaseDatabase.getInstance();

            //Referensi database yang dituju
            DatabaseReference referensi = database.getReference("Nama Pengguna").child(editNama.getText().toString());

            //memberi nilai pada referensi yang dituju
            referensi.child("E-mail").setValue(editEmail.getText().toString());
            referensi.child("Password").setValue(editPass.getText().toString());
            Toast.makeText(Register.this, "Data pengguna baru berhasil ditambahkan", Toast.LENGTH_LONG).show();

            final ProgressDialog progressDialog = ProgressDialog.show(Register
                    .this, "Harap Tunggu...", "Sedang Memproses ...", true);
            (firebaseAuth.createUserWithEmailAndPassword(editEmail.getText().toString(), editPass.getText().toString()))
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressDialog.dismiss();

                            if (task.isSuccessful()) {
                                Toast.makeText(Register.this, "Registrasi Berhasil", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(Register.this, Login.class);
                                startActivity(intent);

                            } else {
                                Log.e("Error", task.getException().toString());
                                Toast.makeText(Register.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });

        }
    }
}
